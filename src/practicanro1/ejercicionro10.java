/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package practicanro1;

import javax.swing.JOptionPane;

/**
 *
 * @author user
 */
public class ejercicionro10 {
    public static void main(String[] args){
          String inputtext = JOptionPane.showInputDialog("Introduc el numero: ");
          int number = Integer.parseInt(inputtext);
          String result = cifras(number);
          JOptionPane.showMessageDialog(null, result);
    }
    public static String cifras(int number){
    
        //941/ 100 =9 
        //(941/10)=94%10=4
        //941%10=1
        int fn = number/100;
        int sn = (number/10)%10;
        int tn = (number%10);
        return "Las sifras son: "+fn+" - "+sn+" - "+tn;
    }
    public static boolean checknumber(int number){
        int count = 0;
        for(int i=number;i>0;i/=10){
            count++;
        }
        if(count == 3){
            return true;
        }
        else{
            return false;
        }
    }
}
