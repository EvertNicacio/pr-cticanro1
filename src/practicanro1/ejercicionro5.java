/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package practicanro1;
import java.util.*;
/**
 *
 * @author EVERT NICACIO PACHECO
 */
public class ejercicionro5 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        double dividendo, divisor;
        System.out.print("INTRODUZCA EL DIVIDENDO: ");
        dividendo = sc.nextDouble();
        System.out.print("INTRODUZCA EL DIVISOR: ");
        divisor = sc.nextDouble();
        if(divisor==0){
            System.out.print("NO SE PUEDE DIVIDIR POR CERO: ");
        }
        else{
            System.out.println(dividendo + " / " + divisor + " = " + dividendo/divisor);
            System.out.printf("%.2f / %.2f = %.2f %n" , dividendo, divisor, dividendo/divisor);
        }
    }
}
