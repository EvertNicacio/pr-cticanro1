/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package practicanro1;
import javax.swing.JOptionPane;
/**
 *
 * @author EVERT NICACIO PACHECO
 */
public class ejercicionro6 {
    public static void main(String[] args){
         String inputtext = JOptionPane.showInputDialog("Introduce un numero");
         int number = Integer.parseInt(inputtext);
         String result = decimaltoBinary(number);
         JOptionPane.showMessageDialog(null, "El numero: "+number+"en binario es: "+result);
    }
    public static String decimaltoBinary(int number){
         String binary = "";
         String dig;
         for(int i=number; i>0; i/=2){
             if(i%2==1){
                 dig = "1";
             }
             else{
                dig ="0";
             }
             binary=dig+binary;
         }
         return binary;
    }
}
