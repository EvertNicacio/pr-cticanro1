/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package practicanro1;

import javax.swing.JOptionPane;
import java.text.DecimalFormat;

/**
 *
 * @author EVERT NICACIO PACHECO
 */
public class ejercicionro8 {
    public static void main(String[] args){
       String inputtext = JOptionPane.showInputDialog("Cantidad de Bolivianos");
       double cant = Double.parseDouble(inputtext);
       String money = JOptionPane.showInputDialog("A que desea convertir");
       JOptionPane.showMessageDialog(null,convert(cant,money));
    }
    public static String convert(double cant,String money){
       double resp = 0;
       switch(money){
           case "Dolares":
               resp = cant * 0.14451;
               break;
           case "Euros":
               resp = cant * 0.12930;
               break;
           case "Libras":
               resp = cant * 0.11552;
               break;
           default:
               JOptionPane.showMessageDialog(null,"Por favor introduzca un convertidor valido");
               break;
       }
       DecimalFormat df = new DecimalFormat("#.00");
       return "La cantidad en: "+money+" es "+df.format(resp);
    }
}
