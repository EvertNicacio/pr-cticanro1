/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package practicanro1;
import javax.swing.JOptionPane;
/**
 *
 * @author EVERT NICACIO PACHECO
 */
public class ejercicionro7 {
    public static void main(String[] args){
       int number;
       String inputtext = JOptionPane.showInputDialog("Introduce un numero");
       number = countNumber(Integer.parseInt(inputtext));
       JOptionPane.showMessageDialog(null, "El nunero de cifras que tiene el numero: "+inputtext+" es: "+ number);
    }
    public static int countNumber(int number){
        int count = 0;
        for(int i = number; i>0; i/=10){
            count++;
        }
        return count;
    }
}
